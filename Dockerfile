FROM nginx:alpine

COPY ./dist /app

COPY ./default.conf /etc/nginx/conf.d/
