function MirrorzParser(j) {
  var linux = [];
  var app = [];
  var other = [];
  j.mirrors.forEach((item) => {
    if (item.type === "linux") linux.push(item);
    else if (item.type === "app") app.push(item);
    else other.push(item);
  });
  return {
    linux: linux,
    app: app,
    other: other,
  };
}
export { MirrorzParser };
