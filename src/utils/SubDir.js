function GetBaseDir(j) {
  var iptv_domains = /(iptv\.uestc\.edu\.cn|subtest\.localhost)/g;
  if (iptv_domains.test(window.location.href)) return "/mirrors/";
  else return "/";
}
export { GetBaseDir };
