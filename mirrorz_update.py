#!/usr/bin/python3

import json
import os
import shutil

DIR = '/mirrors/status'

DSTDIR = '/mirrors/data'

_,_,repos = next(os.walk(DIR))

repo_stats = {}

repo_template = 'template.json'

PUB_URL = "https://垃圾镜像.top"

URL = "http://mirrors.uestc.cn"

def to_status(size, mtime):
    mtime = str(round(mtime))
    if size is 7:
        return "S" + mtime
    if size is 8:
        return "S" + mtime
    return "F" + mtime

for repo in repos:
    stat = os.stat(DIR + '/' + repo)
    repo_stats[repo] = to_status(stat.st_size, stat.st_mtime)

wf = {}

with open(repo_template) as f:
    wf = json.load(f)

wf["site"]["url"] = URL

for idx,repo in enumerate(wf["mirrors"]):
    repo_url = repo["url"].replace("/", "")
    if repo_url in repo_stats:
        wf["mirrors"][idx]["status"] = repo_stats[repo_url]
    if repo["type"] == 'linux':
        rtype = "repo"
    if repo["type"] == 'app':
        rtype = "app"
    wf["mirrors"][idx]["help"] = URL + "/" + rtype + repo["url"]

usage = shutil.disk_usage("/mirrors/data")
wf["disk_usage"] = { "total": usage[0], "used": usage[1] }

with open(DSTDIR + "/mirrorz.json", "w") as f:
    json.dump(wf, f, ensure_ascii=False)
