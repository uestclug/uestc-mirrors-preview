# UESTC Mirrors Frontend

## Info

Frontend of UESTC Mirrors.

## Config Files

### Site Config

- `.env`: Version and mirror url.

### Build Config

- `public/data/news.json`: News preview at the index page.

### Pre-generated Config

- `src/assets/data/doc.json`: Mirrors usage and information documents.
- `src/assets/data/iso.json`: ISO download information.

## TODO

- [x] More documents
- [ ] ISO Download
- [x] News View
- [ ] Status View
- [x] Autorefresh update time and status
- [x] Dist home view
- [X] Apply theme setting to local storage
- [ ] SEO optimize
- [ ] Full i18n.

## Start development

Copy and rename `mirrorz.sample.json` to `/public/mirrorz.json`.

```shell
# Use npm
npm install
npm run serve

# Or use yarn
yarn install
yarn serve
```

## Version

`Year.Month-B(eta)/S(table) (Number)`

Example: `20.09-B1`
