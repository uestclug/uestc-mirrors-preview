## Docker Community Edition 镜像使用帮助

**注意: 本镜像只提供 Debian/Ubuntu/Fedora/CentOS/RHEL 的 docker 软件包，非 dockerhub**

### Debian/Ubuntu 用户

``` bash
# 如果你过去安装过docker，先删除
sudo apt-get remove docker docker-engine docker.io

# 安装依赖
sudo apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common

# 信任公钥
source /etc/os-release
curl -fsSL https://download.docker.com/linux/${ID}/gpg | sudo apt-key add -

# 对于 amd64 架构的计算机
sudo add-apt-repository \
   "deb [arch=amd64] https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/${ID} \
   ${VERSION_CODENAME} \
   stable"

# 如果你是树莓派或其它ARM架构计算机，请运行:
echo "deb [arch=armhf] https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/${ID} \
     ${VERSION_CODENAME} stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list

# 最后安装
sudo apt update
sudo apt install docker-ce
```

### Fedora/CentOS/RHEL

``` bash
# 如果你过去安装过docker，先删除
sudo yum remove docker docker-common docker-selinux docker-engine

# 安装依赖
sudo yum install -y yum-utils device-mapper-persistent-data lvm2

# 下载repo
# Fedora
wget -O /etc/yum.repos.d/docker-ce.repo https://download.docker.com/linux/fedora/docker-ce.repo
# RHEL/CentOS
wget -O /etc/yum.repos.d/docker-ce.repo https://download.docker.com/linux/centos/docker-ce.repo

# 将仓库地址替换为 UESTC Mirror
sudo sed -i 's+download.docker.com+mirrors.uestc.cn/docker-ce+' /etc/yum.repos.d/docker-ce.repo
# 最后安装
sudo yum makecache
sudo yum install docker-ce
```

2021-04-28 附记：
    因内部维护上的一些困难，**本站暂时无法继续提供 HTTPS 服务**，目前恢复时间未定。
    对此给您带来的任何不便我们深感抱歉。