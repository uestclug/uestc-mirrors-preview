## Alpine 镜像使用帮助

Alpine Linux 是一个面向安全, 基于 musl libc 与 busybox 项目的轻量级 Linux 发行版。

在终端输入以下命令以替换 UESTC Mirrors 镜像源： 

``` bash
sed -i 's/dl-cdn.alpinelinux.org/mirrors.uestc.cn/g' /etc/apk/repositories`
```

2021-04-28 附记：
    因内部维护上的一些困难，**本站暂时无法继续提供 HTTPS 服务**，目前恢复时间未定。
    对此给您带来的任何不便我们深感抱歉。