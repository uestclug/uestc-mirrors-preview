## CentOS 镜像使用帮助

首先备份 `CentOS-Base.repo` 
``` bash
sudo cp /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bak
```

之后启用 UESTC Mirrors 软件仓库

``` bash
sed -i  -e 's/mirrorlist/#mirrorlist/g'\
        -e 's/^#baseurl/baseurl/g' \
#        -e 's/http:/https:/g' \
        -e 's/mirror.centos.org/mirrors.uestc.cn/g' \
        /etc/yum.repos.d/CentOS-Base.repo
```

更新软件包缓存 `sudo yum makecache`

2021-04-28 附记：
    因内部维护上的一些困难，**本站暂时无法继续提供 HTTPS 服务**，目前恢复时间未定。
    对此给您带来的任何不便我们深感抱歉。