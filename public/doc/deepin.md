## Deepin 镜像使用帮助

首先备份 `/etc/apt/sources.list`

``` bash
cp /etc/apt/sources.list /etc/apt/sources.list.bak
```

启用 UESTC 镜像

``` bash
sed -i  
#-e 's/http:/https:/g' \
        -e 's/packages.deepin.com/mirrors.uestc.cn/g' \
        /etc/apt/sources.list
```

如果遇到无法拉取 https 源的情况，请先使用 http 源并安装：

``` shell
$ sudo apt install apt-transport-https
```

再使用 UESTC Mirrors 的软件源镜像。

2021-04-28 附记：
    因内部维护上的一些困难，**本站暂时无法继续提供 HTTPS 服务**，目前恢复时间未定。
    对此给您带来的任何不便我们深感抱歉。