## Arch Linux 软件仓库镜像使用帮助

编辑 `/etc/pacman.d/mirrorlist`， 在文件的最顶端添加： 

``` none
Server = http://mirrors.uestc.cn/archlinux/$repo/os/$arch
```

更新软件包缓存： `sudo pacman -Syy`


2021-04-28 附记：
    因内部维护上的一些困难，**本站暂时无法继续提供 HTTPS 服务**，目前恢复时间未定。
    对此给您带来的任何不便我们深感抱歉。
    