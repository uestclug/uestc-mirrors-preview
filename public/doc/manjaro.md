## Manjaro 软件仓库镜像使用帮助

编辑 `/etc/pacman.d/mirrorlist`， 在文件的最顶端添加：

``` none
Server = http://mirrors.uestc.cn/manjaro/stable/$repo/$arch
```

更新软件包缓存： `sudo pacman -Syy`

`pacman-mirrors` 等命令会自动生成镜像列表并覆盖你的改动，请避免使用

2021-04-28 附记：
    因内部维护上的一些困难，**本站暂时无法继续提供 HTTPS 服务**，目前恢复时间未定。
    对此给您带来的任何不便我们深感抱歉。