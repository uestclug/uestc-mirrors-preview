### F-Droid 镜像使用帮助

##### F-Droid 仓库添加方式

``` none
http://mirrors.uestc.cn/fdroid/repo/?fingerprint=43238D512C1E5EB2D6569F4A3AFBF5523418B82E0A3ED1552770ABB9A9C9CCAB
```

使用客户端打开或在设置 -> 仓库中手动添加上述 URL，即可将此镜像添加为用户镜像。
*用户可能需要手动关闭此仓库的其他源以获得最佳效果。

##### F-Droid Archive 仓库添加方式

``` none
http://mirrors.uestc.cn/fdroid/archive?fingerprint=43238D512C1E5EB2D6569F4A3AFBF5523418B82E0A3ED1552770ABB9A9C9CCAB
```

使用客户端打开或在设置 -> 仓库中手动添加上述 URL，即可将此镜像添加为用户镜像。
*用户可能需要手动关闭此仓库的其他源以获得最佳效果。