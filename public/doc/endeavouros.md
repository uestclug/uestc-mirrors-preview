## EndeavourOS 软件仓库镜像使用帮助

> 作为 Arch Linux 的衍生发行版， EndeavourOS 依赖上游生态和软件仓库来完成大部分功能，而其自家仓库只维护较少的特色软件包。

> 您可能需要参考本站的 Arch Linux 软件仓库镜像使用帮助来完成配置，其同样适用于 EndeavourOS. 

##### 添加 EndeavourOS 特色软件包仓库

编辑 `/etc/pacman.d/endeavouros-mirrorlist`， 在文件的最顶端添加： 

``` none
Server = http://mirrors.uestc.cn/endeavouros/$repo/endeavouros/$arch
```

更新软件包缓存： `sudo pacman -Syy`


2021-04-28 附记：
    因内部维护上的一些困难，**本站暂时无法继续提供 HTTPS 服务**，目前恢复时间未定。
    对此给您带来的任何不便我们深感抱歉。