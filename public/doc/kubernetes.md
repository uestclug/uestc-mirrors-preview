## Debian/Ubuntu 用户

首先导入 gpg key：

``` shell
$ curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
```

然后添加镜像源

``` bash
source /etc/os-release
echo "http://mirror.uestc.cn/kubernetes/apt kubernetes-${VERSION_CODENAME} main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt update
```

## RHEL/CentOS 用户

新建 `/etc/yum.repos.d/kubernetes.repo`，内容为：

``` none
[kubernetes]
name=kubernetes
baseurl=http://mirrrors.uestc.cn/kubernetes/yum/repos/kubernetes-el7-$basearch
enabled=1
```
2021-04-28 附记：
    因内部维护上的一些困难，**本站暂时无法继续提供 HTTPS 服务**，目前恢复时间未定。
    对此给您带来的任何不便我们深感抱歉。