## Arch Linux Repo-CK 镜像使用帮助

`repo-ck` 是 Arch 的非官方仓库，内有包含 ck 补丁、BFS 调度器等通用或为特定 CPU 架构优化过的内核及相关软件包，是居家旅行、优化折腾的必备良药。更多内容请参考 [Arch Wiki](https://wiki.archlinux.org/index.php/repo-ck) 的相关页面。

食用方法
在 `/etc/pacman.conf` 里添加

``` none
[repo-ck]							
Server = http://mirrors.uestc.cn/repo-ck/$arch
```

再增加 GPG 信任:

``` bash
pacman-key -r 5EE46C4C && pacman-key --lsign-key 5EE46C4C
```

之后 `pacman -Sy` 即可。

2021-04-28 附记：
    因内部维护上的一些困难，**本站暂时无法继续提供 HTTPS 服务**，目前恢复时间未定。
    对此给您带来的任何不便我们深感抱歉。